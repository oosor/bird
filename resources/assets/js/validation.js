/**
 * class for ValidatorGroup or single validation
 * */
export class Validator {

    /** Validator for email
     * @param {string} email
     * @return {boolean}
     * */
    static email(email) {
        return /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(email);
    }

    /** Validator for phone number
     * @param {string} number
     * @return {boolean}
     * */
    static phone(number) {
        return /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/.test(number);
    }

}

/**
 * simple class for group Validators
 * */
export class ValidatorGroup {

    /**
     * @typedef {Object<HTMLElement, function>} ValidatorOne
     * @property {HTMLElement} dom
     * @property {function} validator
     *
     * @param {ValidatorOne[]} validators
     * */
    constructor(validators) {
        this.validators = validators;
    }

    /** validator from group if null -> valid or array object error
     * @typedef {Object<boolean, string>} ValidatorError
     * @property {boolean} invalid
     * @property {string} type
     *
     * @return {!ValidatorError[]}
     * */
    invalid() {
        return this.validators.reduce((error, item) => {
            if (item.validator(item.dom.value)) {
                return error;
            }
            const validatorError = {invalid: true, type: item.validator.name};
            error ? error.push(validatorError) : (error = [validatorError]);
            return error;
        }, null);
    }
}