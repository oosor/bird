import '../css/app.scss';

import {Validator, ValidatorGroup} from './validation';
import {OrderHttp} from "./order-http";


const data = {
    name: document.querySelector('#validationServerUsername'),
    email: document.querySelector('#validationServerEmail'),
    phone: document.querySelector('#validationServerPhone'),
};
const buttonForm = document.querySelector('#buttonForm');
const successStatus = document.querySelector('#successResult');
const failedStatus = document.querySelector('#failedResult');
const spinner = document.querySelector('#spinner');


// fn onChange validation input value
const changeValid = () => {
    const invalid = validGroup.invalid();

    data.email.classList.toggle('is-invalid', invalid ? invalid.some(error => error.type === 'email') : false);
    data.phone.classList.toggle('is-invalid', invalid ? invalid.some(error => error.type === 'phone') : false);
    !invalid ? buttonForm.removeAttribute('disabled') : buttonForm.setAttribute('disabled', 'disabled');
};

// create validator group
const validGroup = new ValidatorGroup([
    {dom: data.email, validator: Validator.email},
    {dom: data.phone, validator: Validator.phone},
]);

const orderHttp = new OrderHttp();

changeValid();

// send data to server
const sendData = async () => {
    buttonForm.setAttribute('disabled', 'disabled');
    spinner.style.display = 'block';

    const params = {
        name: data.name.value,
        email: data.email.value,
        phone: data.phone.value,
    };
    const returnData = await orderHttp.create(params);
    if (returnData && returnData.status) {
        successStatus.style.display = 'block';
    } else {
        failedStatus.style.display = 'block';
    }

    setTimeout(() => {
        successStatus.style.display = 'none';
        failedStatus.style.display = 'none';
    }, 3000);

    buttonForm.removeAttribute('disabled');
    spinner.style.display = 'none';
};

data.email.addEventListener('input', changeValid);
data.phone.addEventListener('input', changeValid);
buttonForm.addEventListener('click', sendData);