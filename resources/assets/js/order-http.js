import * as axios from 'axios';

const host = window.location.protocol + '//' + window.location.host;

export class OrderHttp {

    constructor() {
        this._httpClient = axios;
    }

    /** create order
     * @param {object} data
     * @return {Promise}
     * */
    async create(data) {
        const result = await this._httpClient.post(host + '/user/create', data);
        return result ? result.data : null;
    }
}