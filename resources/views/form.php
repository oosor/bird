<div class="row justify-content-center">
    <div class="col-lg-5">
        <div class="card text-white bg-info mt-3">
            <div class="card-header">Create order</div>
            <div class="card-body">
                <form>
                    <div class="form-row">
                        <div class="col-12 mb-3">
                            <label for="validationServerUsername">Name</label>
                            <div class="input-group border border-light rounded" id="nameFormGroup">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend1"><i class="fa fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control" id="validationServerUsername" placeholder="Name" aria-describedby="inputGroupPrepend1">
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <label for="validationServerUsername">Email</label>
                            <div class="input-group border border-light rounded" id="emailFormGroup">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-at"></i></span>
                                </div>
                                <input type="text" class="form-control" id="validationServerEmail" placeholder="Email" aria-describedby="inputGroupPrepend2" required>
                                <div class="invalid-feedback">
                                    Please choose a valid email.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <label for="validationServerUsername">Phone</label>
                            <div class="input-group border border-light rounded" id="phoneFormGroup">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend3"><i class="fa fa-phone"></i></span>
                                </div>
                                <input type="text" class="form-control" id="validationServerPhone" placeholder="Phone" aria-describedby="inputGroupPrepend3" required>
                                <div class="invalid-feedback">
                                    Please choose a valid phone number.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 d-flex justify-content-end">
                            <div class="result">
                                <i id="successResult" class="fa fa-check"></i>
                                <i id="failedResult" class="fa fa-close"></i>
                                <i id="spinner" class="fa fa-spinner fa-spin fa-pulse"></i>
                            </div>
                            <button class="btn border border-light btn-info" id="buttonForm" type="button">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>