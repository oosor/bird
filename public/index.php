<?php

ini_set('display_errors', 1);
define('ROOT_PATH', __DIR__.'/..');

require ROOT_PATH.'/vendor/autoload.php';
require_once ROOT_PATH . '/App/Core/App.php';

\App\Core\App::init();
