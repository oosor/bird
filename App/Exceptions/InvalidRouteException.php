<?php

namespace App\Exceptions;


class InvalidRouteException extends \Exception
{

    public function __toString() {
        $errorMsg = 'Error on line '.$this->getLine().' in '.$this->getFile().': '.$this->getMessage();
        return $errorMsg;
    }
}