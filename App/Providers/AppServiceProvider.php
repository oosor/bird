<?php

namespace App\Providers;


use App\Core\Provider;
use App\Services\Amocrm\AmocrmService;
use App\Services\Amocrm\MicroServices\{
    AccountService, ContactService, DealService, TaskService
};
use App\Services\Work\AmoService;
use GuzzleHttp\Client;

class AppServiceProvider extends Provider
{

    /**
     * the boot method from services injections
     * */
    public function boot()
    {
        $client = new Client(['cookies' => true]);
        $dealService = new DealService($client);
        $accountService = new AccountService($client);
        $contactService = new ContactService($client);
        $taskService = new TaskService($client);
        $this->inject('App\Services\Amocrm\AmocrmService', new AmocrmService($dealService, $accountService, $contactService, $taskService));

        $this->inject('App\Services\Work\AmoService', new AmoService($this->{'App\Services\Amocrm\AmocrmService'}));
    }
}