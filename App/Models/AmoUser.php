<?php

namespace App\Models;


use App\Core\Model;
use App\Services\Work\Contracts\AmoModel;

/**
 * @property integer id
 * @property string name
 * @property string login
 * @property bool is_active
 * @property bool is_admin
 * @property array right
 * @property integer count_deals_for_day
 * ... and more
 * */
class AmoUser extends Model implements AmoModel
{

    public function toServer(): array
    {
        // TODO: Implement toServer() method.
    }
}