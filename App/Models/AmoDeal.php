<?php

namespace App\Models;


use App\Core\Model;
use App\Services\Work\Contracts\AmoModel;

/**
 * @property integer id
 * @property string name
 * @property integer responsible_user_id
 * @property integer created_by
 * @property integer created_at
 * @property integer updated_at
 * @property integer account_id
 * @property integer pipeline_id
 * @property integer contacts_id
 * @property array main_contact
 * ... and more
 * */
class AmoDeal extends Model implements AmoModel
{

    public function toServer(): array
    {
        return $this->toArray();
    }
}