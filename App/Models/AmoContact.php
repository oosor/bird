<?php

namespace App\Models;


use App\Core\Model;
use App\Services\Work\Contracts\AmoModel;

/**
 * @property integer id
 * @property string name
 * @property integer responsible_user_id
 * @property integer created_by
 * @property integer created_at
 * @property integer updated_at
 * @property integer account_id
 * @property integer group_id
 * @property array custom_fields
 * ... and more
 * */
class AmoContact extends Model implements AmoModel
{

    /** create contact from User model data
     * @param User $user
     * @return $this
     * */
    public static function fromUser(User $user)
    {
        if ($user) {
            $model = new AmoContact();
            $name= $user->name;
            $model->name = empty($name) ? 'No name' : $name;
            $model->custom_fields = [
                ['id' => 256843, 'values' => [['value' => $user->email, 'enum' => 370751]]],
                ['id' => 256841, 'values' => [['value' => $user->phone, 'enum' => 370739]]],
            ];

            return $model;
        }
        return null;
    }

    public function toServer(): array
    {
        return $this->toArray();
    }
}