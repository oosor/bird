<?php

namespace App\Models;


use App\Core\Model;
use App\Services\Work\Contracts\AmoModel;

/**
 * @property integer id
 * @property integer responsible_user_id
 * @property integer element_id
 * @property integer element_type
 * @property integer created_by
 * @property integer created_at
 * @property integer updated_at
 * @property integer account_id
 * @property integer task_type
 * @property bool is_completed
 * @property integer complete_till_at
 * @property string text
 * ... and more
 * */
class AmoTask extends Model implements AmoModel
{

    public function toServer(): array
    {
        return $this->toArray();
    }
}