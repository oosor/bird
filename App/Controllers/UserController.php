<?php

namespace App\Controllers;


use App\Core\Controller;
use App\Models\User;
use App\Services\Work\AmoService;

class UserController extends Controller
{

    private $amoService;

    /**
     * @param AmoService $amoService
     * */
    function __construct(AmoService $amoService)
    {
        parent::__construct();
        $this->amoService = $amoService;
    }

    /**
     * @param array $data injected get or post data
     * */
    public function create($data)
    {
        if (isset($data)) {
            $user = new User();
            $user->name = $data['name'] ?? null;
            $user->email = $data['email'] ?? null;
            $user->phone = $data['phone'] ?? null;
            // is equivalent to -> $user = new User($data);

            try {
                $this->amoService->createOrderFromUser($user);

                $bodyMail = "Имя пользователя: " . $user->name . "\n\r";
                $bodyMail .= "Електронная почта: " . $user->email . "\n\r";
                $bodyMail .= "Телефон: " . $user->phone . "\n\r";
                $this->amoService->sendMailAdmin($bodyMail);
            } catch (\Exception $exception) {
                echo json_encode(['status' => false, 'error' => 'Server error']);
                return;
            }

            echo json_encode(['status' => true, 'data' => []]);

            return;
        }

        echo json_encode(['status' => false, 'error' => 'Invalid request']);
    }
}