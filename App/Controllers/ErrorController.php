<?php

namespace App\Controllers;


use App\Core\Controller;

class ErrorController extends Controller
{

    public function error404($error)
    {
        $this->view->render('errors.404', null, 'layouts.empty');
    }

    public function error500($error)
    {
        $this->view->render('errors.500', null, 'layouts.empty');
    }
}