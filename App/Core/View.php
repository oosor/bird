<?php

namespace App\Core;


class View
{
    public static $pathViews = ROOT_PATH.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;

    /**
     * @param string $contentView
     * @param mixed $data
     * @param string $layoutView
     * */
    function render($contentView, $data = null, $layoutView = 'layouts.layout')
    {
        $layoutView = str_replace('.', DIRECTORY_SEPARATOR, $layoutView).'.php';
        $contentView = str_replace('.', DIRECTORY_SEPARATOR, $contentView).'.php';

        include static::$pathViews.$layoutView;
    }
}