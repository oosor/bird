<?php

namespace App\Core;


class Model
{
    private $values = [];

    /**
     * @param array $values
     * */
    public function __construct($values = null)
    {
        if ($values & is_array($values)) {
            $this->values = $values;
        }
    }

    public function __set($name, $value)
    {
        $this->values[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->values)) {
            return $this->values[$name];
        }

        return null;
    }

    public function toArray()
    {
        return $this->values;
    }

    public function __toString()
    {
        return json_encode($this->values);
    }

}