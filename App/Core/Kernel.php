<?php

namespace App\Core;


use App\Exceptions\InvalidRouteException;
use App\Providers\AppServiceProvider;

class Kernel
{
    public $defaultControllerName = 'Home';
    public $defaultActionName = "index";
    public static $pathToControllers = ROOT_PATH . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR;
    public static $nameSpace = '\\App\\Controllers\\';

    /**
     * load controller
     * @throws InvalidRouteException
     * */
    public function launch()
    {
        list($controllerName, $actionName, $params) = App::$router->resolve();
        return $this->launchAction($controllerName, $actionName, $params);
    }


    /**
     * @param string $controllerName
     * @param string $actionName
     * @param array $params
     * @return mixed
     * @throws InvalidRouteException
     * */
    public function launchAction($controllerName, $actionName, $params)
    {

        $controllerName = (empty($controllerName) ? $this->defaultControllerName : ucfirst($controllerName)) . 'Controller';
        if(!file_exists(static::$pathToControllers . $controllerName . '.php')){
            throw new InvalidRouteException();
        }
        require_once static::$pathToControllers . $controllerName . '.php';
        if(!class_exists(static::$nameSpace . ucfirst($controllerName))){
            throw new InvalidRouteException();
        }
        $controllerName = static::$nameSpace . ucfirst($controllerName);
        $provider = new AppServiceProvider();
        $controller = $provider->getInjections($controllerName);
        $actionName = empty($actionName) ? $this->defaultActionName : $actionName;
        if (!method_exists($controller, $actionName)){
            throw new InvalidRouteException();
        }

        return $controller->$actionName($this->getData(), $params);
    }

    /** get or post params
     * @return array
     * */
    protected function getData()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                $params = explode('&', $_SERVER['QUERY_STRING'] ?? '');
                if (!empty($params) && !empty($params[0])) {
                    return array_reduce($params, function ($array, $item) {
                        $oneElement = explode('=', $item, 2);
                        $array[$oneElement[0]] = $oneElement[1];
                        return $array;
                    }, []);
                }
                return null;
            case 'POST':
                $data = file_get_contents('php://input') ?? null;
                return isset($data) ? json_decode($data, true) : null;
        }
        return null;
    }
}