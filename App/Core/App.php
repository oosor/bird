<?php

namespace App\Core;


use App\Exceptions\InvalidRouteException;
use Dotenv\Dotenv;

class App
{
    /**
     * @var Router $router
     * */
    public static $router;
    /**
     * @var Kernel $kernel
     * */
    public static $kernel;

    public static function init()
    {
        (Dotenv::create(ROOT_PATH))->load();
        spl_autoload_register(['static', 'loadClass']);
        static::bootstrap();
        set_exception_handler([__NAMESPACE__ . '\\' . 'App', 'handleException']);

        static::$kernel->launch();
    }

    public static function bootstrap()
    {
        static::$router = new Router();
        static::$kernel = new Kernel();
    }

    public static function loadClass($className)
    {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        require ROOT_PATH . DIRECTORY_SEPARATOR . $className . '.php';

    }

    public function handleException(\Throwable $throwable)
    {
        if ($throwable instanceof InvalidRouteException) {
            echo static::$kernel->launchAction('Error', 'error404', [$throwable]);
        } else {
            echo static::$kernel->launchAction('Error', 'error500', [$throwable]);
        }
    }
}