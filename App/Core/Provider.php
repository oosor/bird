<?php

namespace App\Core;


use App\Providers\Contracts\ProviderContract;

abstract class Provider
{
    protected static $providers = [];

    public function __construct()
    {
        static::boot();
    }

    public abstract function boot();

    /** inject service provider
     * @param string $path
     * @param ProviderContract $provider
     * */
    public function inject($path, ProviderContract $provider)
    {
        static::$providers[$path] = $provider;
    }

    /** get dependency injections if was injected in ServiceProvider
     * @param string $name
     * @return ProviderContract
     * */
    public function __get($name)
    {
        if (array_key_exists($name, static::$providers)) {
            return static::$providers[$name];
        }

        return null;
    }


    /** get context controller or other class with dependency injections
     * @param string $controllerName
     * @return mixed
     * */
    public function getInjections($controllerName)
    {
        try {
            $class = new \ReflectionClass($controllerName);

            $dependencies = array_map(function ($item) {
                return $this->{$item->getClass()->getName()} ?? $item->getClass()->newInstance();
            }, $class->getConstructor()->getParameters());

            return $class->newInstanceArgs($dependencies);
        } catch (\ReflectionException $reflectionException) {
            return new $controllerName;
        }
    }
}