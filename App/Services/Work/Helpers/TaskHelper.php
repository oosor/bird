<?php

namespace App\Services\Work\Helpers;


use App\Models\AmoTask;
use App\Services\Work\Contracts\AmoModel;

trait TaskHelper
{

    /**
     * @param array $params
     * @return AmoTask[]
     * */
    public function getTasks($params = null)
    {
        $rawData = $this->amocrmService->getTasks($params);

        try {
            if (!empty($rawData)) {

                return array_map(function ($item) {
                    return new AmoTask($item);
                }, array_values($rawData['_embedded']['items']));
            }
        } catch (\Exception $exception) { }

        return null;
    }

    /**
     * @param AmoModel $amoTask
     * @return integer
     * */
    public function createTask(AmoModel $amoTask)
    {
        if (isset($amoTask)) {
            $rawData = $this->amocrmService->createTask([$amoTask->toServer()]);

            if (isset($rawData, $rawData['_embedded'])) {
                $item = array_shift($rawData['_embedded']['items']);
                return $item['id'] ?? null;
            }
        }

        return null;
    }
}