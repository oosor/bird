<?php

namespace App\Services\Work\Helpers;


use App\Models\AmoDeal;
use App\Models\AmoUser;

trait DealHelper
{

    /**
     * @param AmoUser $amoUser
     * @return AmoDeal[]
     * */
    public function getDealsFromUser(AmoUser $amoUser)
    {
        $rawData = $this->amocrmService->getDeals([
            'responsible_user_id' => $amoUser->id,
        ]);

        return $this->_amoDealHelper($rawData);
    }

    /**
     * @param array $params
     * @return AmoDeal[]
     * */
    public function getDeals($params = null)
    {
        $rawData = $this->amocrmService->getDeals($params);

        return $this->_amoDealHelper($rawData);
    }

    /**
     * @param AmoDeal $amoDeal
     * @return integer
     * */
    public function newDeal(AmoDeal $amoDeal)
    {
        if (isset($amoDeal)) {
            $rawData = $this->amocrmService->newDeal([$amoDeal->toServer()]);

            if (isset($rawData, $rawData['_embedded'])) {
                $item = array_shift($rawData['_embedded']['items']);
                return $item['id'] ?? null;
            }
        }

        return null;
    }


    /**
     * @param array $rawData
     * @return AmoDeal[]
     * */
    private function _amoDealHelper($rawData)
    {
        try {
            if (!empty($rawData)) {

                return array_map(function ($item) {
                    return new AmoDeal($item);
                }, array_values($rawData['_embedded']['items']));
            }
        } catch (\Exception $exception) { }

        return null;
    }
}