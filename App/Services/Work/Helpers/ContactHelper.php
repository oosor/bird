<?php

namespace App\Services\Work\Helpers;


use App\Models\AmoContact;
use App\Services\Work\Contracts\AmoModel;

trait ContactHelper
{
    
    /** collect contacts
     * @param array $params
     * @param string $query
     * @return AmoContact[]
     * */
    public function getContacts($params = null, $query = null)
    {

        if (isset($params)) {
            foreach ($params as $param) {
                $response = $this->getContacts(null, $param);
                if ($response && !empty($response)) {
                    return $response;
                }
            }
            return null;
        }

        $rawData = $this->amocrmService->getContacts(['query' => $query]);

        try {
            if (!empty($rawData)) {

                return array_map(function ($item) {
                    return new AmoContact($item);
                }, array_values($rawData['_embedded']['items']));
            }
        } catch (\Exception $exception) { }

        return null;
    }

    /** create new contact and return id of success
     * @param AmoModel $amoContact
     * @return integer
     * */
    public function createContact(AmoModel $amoContact)
    {
        if (isset($amoContact)) {
            $rawData = $this->amocrmService->createContact([$amoContact->toServer()]);

            if (isset($rawData, $rawData['_embedded'])) {
                $item = array_shift($rawData['_embedded']['items']);
                return $item['id'] ?? null;
            }
        }

        return null;
    }
}