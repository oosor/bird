<?php

namespace App\Services\Work\Helpers;


use App\Models\AmoUser;

trait AccountHelper
{


    /** collect AmoUser
     * @return AmoUser[]
     * */
    public function getUsers()
    {
        $rawData = $this->amocrmService->getUsers();

        try {
            if (!empty($rawData)) {

                return array_map(function ($item) {
                    return new AmoUser($item);
                }, array_values($rawData['_embedded']['users']));
            }
        } catch (\Exception $exception) { }

        return null;
    }

}