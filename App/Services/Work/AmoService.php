<?php

namespace App\Services\Work;


use App\Models\{
    AmoContact, AmoDeal, AmoTask, AmoUser, User
};
use App\Providers\Contracts\ProviderContract;
use App\Services\Amocrm\AmocrmService;
use App\Services\Work\Helpers\{
    AccountHelper, ContactHelper, DealHelper, TaskHelper
};

/**
 * custom service provider
 * */
class AmoService implements ProviderContract
{
    use AccountHelper, DealHelper, ContactHelper, TaskHelper;

    /** @var AmocrmService */
    private $amocrmService;

    /**
     * @param AmocrmService $amocrmService
     * */
    public function __construct(AmocrmService $amocrmService)
    {
        $this->amocrmService = $amocrmService;
    }

    /** custom method from create order from form
     * @param User $user
     * @return bool
     * @throws \Exception
     * */
    public function createOrderFromUser(User $user)
    {
        // search contact for email
        $amoContact = $this->getContacts(['email' => $user->email]);
        if (is_null($amoContact)) {
            // if null search contact for phone
            $amoContact = $this->getContacts(['phone' => $user->phone]);
        }
        if (is_null($amoContact)) {
            // if null create new contact
            $amoContact = AmoContact::fromUser($user);
            $amoUser = $this->getUserWithMinDealsForDay();

            if (isset($amoUser)) {
                $amoContact->responsible_user_id = $amoUser->id;
                $idContact = $this->createContact($amoContact);
                $amoContact->id = $idContact;
            }
        }

        if (is_null($amoContact)) {
            throw new \Exception('Error. Contact not found or not created');
        }
        $amoContact = is_array($amoContact) ? array_shift($amoContact) : $amoContact;

        // create task
        $amoTask = new AmoTask();
        $amoTask->complete_till_at = time() + 24 * 60 * 60;
        $amoTask->responsible_user_id = $amoContact->responsible_user_id;
        $amoTask->task_type = 1;
        $amoTask->element_id = $amoContact->id;
        $amoTask->element_type = 1;
        $amoTask->text = 'Перезвонить клиенту';
        if (is_null($this->createTask($amoTask))) {
            throw new \Exception('Error. Task not created');
        }

        // create deal
        $amoDeal = new AmoDeal();
        $amoDeal->name = 'Заявка с сайта';
        $amoDeal->responsible_user_id = $amoContact->responsible_user_id;
        $amoDeal->contacts_id = $amoContact->id;
        if (is_null($this->newDeal($amoDeal))) {
            throw new \Exception('Error. Deal not created');
        }

        return true;
    }

    /** method for get responsible user
     * @return AmoUser
     * */
    public function getUserWithMinDealsForDay()
    {
        $amoUsers = $this->getUsers();

        if (is_null($amoUsers)) {
            return null;
        }
        // reject admins
        $amoUsers = array_filter($amoUsers, function ($item) {
           return !$item->is_admin;
        });

        // get Deals from current day
        $amoDeals = $this->getDeals([
            'filter' => [
                'date_create' => ['from' => mktime(0,0,0), 'to' => time()],
            ],
        ]);

        // if deals collect is null return first user exclude admin
        if (is_null($amoDeals)) {
            return array_shift($amoUsers);
        }

        $idsCollect = array_reduce($amoDeals, function ($array, $item) use ($amoUsers) {
            // filter user for not found
            $filteredUsers = array_filter($amoUsers, function ($one) use ($item) {
                return $item->responsible_user_id == $one->id;
            });
            if (empty($filteredUsers) || empty($item->main_contact['id'])) {
                return $array;
            }
            if (isset($array[$item->responsible_user_id])) {
                $array[$item->responsible_user_id][] = $item->main_contact['id'];
            }

            return $array;
        }, array_reduce($amoUsers, function ($array, $item) {
            $array[$item->id] = [];
            return $array;
            }, [])
        );

        $min = PHP_INT_MAX;
        $id = null;
        foreach ($idsCollect as $idUser => $idsDeal) {
            if (count(array_unique($idsDeal)) < $min) {
                $id = $idUser;
                $min = count(array_unique($idsDeal));
            }
        }

        $inArray = array_filter($amoUsers, function ($item) use ($id) {
            return $item->id == $id;
        });

        return array_shift($inArray);
    }


    /** send simple email to admin
     * @param string $body
     * @return bool
     * */
    public function sendMailAdmin($body)
    {
        $subject = "Заявка с сайта";
        $headers = "From: <" . $_ENV['LOCAL_MAIL'] . ">\r\n";
        $headers .= "Content-type: text/plain; charset=utf-8\r\n";
        $headers .="Content-transfer-encoding: quoted-printable\n";

        return mail($_ENV['ADMIN_MAIL'], $subject, $body, $headers);
    }
}