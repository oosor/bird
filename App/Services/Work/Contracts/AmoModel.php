<?php

namespace App\Services\Work\Contracts;


interface AmoModel
{

    public function toServer(): array;
}