<?php

namespace App\Services\Amocrm;


use App\Providers\Contracts\ProviderContract;
use App\Services\Amocrm\MicroServices\{
    AccountService, ContactService, DealService, TaskService
};

/**
 *
 * Deal methods
 * @method newDeal(array $collect): array
 * @method getDeals(array $params): array
 *
 * Account methods
 * @method getDetailAccount(): array
 * @method getUsers(): array
 * @method getPipeLines(): array
 * @method getGroups(): array
 *
 * Contacts methods
 * @method getContacts(array $params = null): array
 * @method createContact(array $param): array
 *
 * Task methods
 * @method getTasks(array $params = null): array
 * @method createTask(array $params): array
 * */
class AmocrmService implements ProviderContract
{

    private $dealService;
    private $accountService;
    private $contactService;
    private $taskService;

    /**
     * @param DealService $dealService
     * @param AccountService $accountService
     * @param ContactService $contactService
     * @param TaskService $taskService
     * */
    public function __construct(DealService $dealService,
                                AccountService $accountService,
                                ContactService $contactService,
                                TaskService $taskService)
    {
        $this->dealService = $dealService;
        $this->accountService = $accountService;
        $this->contactService = $contactService;
        $this->taskService = $taskService;
    }

    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'newDeal':case 'getDeals':
                return $this->dealService->$name(...$arguments);
            case 'getDetailAccount':case 'getUsers':case 'getPipeLines':case 'getGroups':
                return $this->accountService->$name(...$arguments);
            case 'getContacts':case 'createContact':
                return $this->contactService->$name(...$arguments);
            case 'getTasks':case 'createTask':
                return $this->taskService->$name(...$arguments);
        }

        return null;
    }
}