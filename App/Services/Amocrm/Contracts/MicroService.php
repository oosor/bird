<?php

namespace App\Services\Amocrm\Contracts;


use GuzzleHttp\Client;

interface MicroService
{
    public function __construct(Client $client);
}