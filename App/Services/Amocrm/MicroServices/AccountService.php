<?php

namespace App\Services\Amocrm\MicroServices;


use App\Services\Amocrm\Contracts\MicroService;
use App\Services\Amocrm\Helpers\RequestHelper;

class AccountService implements MicroService
{
    use RequestHelper;

    public function getDetailAccount()
    {
        return $this->getArray($this->_getDetail('custom_fields'));
    }

    public function getUsers()
    {
        return $this->getArray($this->_getDetail('users'));
    }

    public function getPipeLines()
    {
        return $this->getArray($this->_getDetail('pipelines'));
    }

    public function getGroups()
    {
        return $this->getArray($this->_getDetail('groups'));
    }

    /** Universal account request
     * @param string $type
     * @return mixed
     * */
    private function _getDetail($type)
    {
        return $this->request('GET', $_ENV['AMO_ACCOUNT_LINK'], [
            'with' => $type,
        ]);
    }
}