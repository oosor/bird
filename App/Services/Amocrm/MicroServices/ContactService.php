<?php

namespace App\Services\Amocrm\MicroServices;


use App\Services\Amocrm\Contracts\MicroService;
use App\Services\Amocrm\Helpers\RequestHelper;

class ContactService implements MicroService
{
    use RequestHelper;

    /**
     * @param array $params
     * @return array
     * */
    public function getContacts($params = null)
    {
        $response = $this->request('GET', $_ENV['AMO_CONTACTS_LINK'], $params);

        return $this->getArray($response);
    }

    /**
     * @param array $params
     * @return array
     * */
    public function createContact($params)
    {
        $response = $this->request('POST', $_ENV['AMO_CONTACTS_LINK'], ['add' => $params]);

        return $this->getArray($response);
    }
}