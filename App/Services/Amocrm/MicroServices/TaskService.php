<?php

namespace App\Services\Amocrm\MicroServices;


use App\Services\Amocrm\Contracts\MicroService;
use App\Services\Amocrm\Helpers\RequestHelper;

class TaskService implements MicroService
{
    use RequestHelper;


    /**
     * @param array $params
     * @return array
     * */
    public function getTasks($params = null)
    {
        $response = $this->request('GET', $_ENV['AMO_TASKS_LINK'], $params);

        return $this->getArray($response);
    }

    /**
     * @param array $params
     * @return array
     * */
    public function createTask($params)
    {
        $response = $this->request('POST', $_ENV['AMO_TASKS_LINK'], ['add' => $params]);

        return $this->getArray($response);
    }
}