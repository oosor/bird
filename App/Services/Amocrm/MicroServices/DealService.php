<?php

namespace App\Services\Amocrm\MicroServices;


use App\Services\Amocrm\Contracts\MicroService;
use App\Services\Amocrm\Helpers\RequestHelper;

class DealService implements MicroService
{
    use RequestHelper;

    public function newDeal($collect)
    {
        $response = $this->request('POST', $_ENV['AMO_DEAL_CREATE_LINK'], ['add' => $collect]);

        return $this->getArray($response);
    }

    public function getDeals($params = null)
    {
        $response = $this->request('GET', $_ENV['AMO_DEALS_LINK'], $params);

        return $this->getArray($response);
    }

}