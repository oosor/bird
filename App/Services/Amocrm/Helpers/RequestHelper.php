<?php

namespace App\Services\Amocrm\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;

trait RequestHelper
{

    protected $cookies;
    private $cookieFile = ROOT_PATH . DIRECTORY_SEPARATOR . 'cookie_jar.txt';
    private $attempt = true;
    private $host;

    private $httpClient;

    /**
     * @param Client $client
     * */
    public function __construct(Client $client)
    {
        $this->httpClient = $client;
        $this->host = $_ENV['AMO_HOST'];
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $data
     * @return \GuzzleHttp\Psr7\Response
     * */
    public function request($method, $link, $data)
    {
        $this->cookies = new FileCookieJar($this->cookieFile, true);

        try {
            return $this->httpClient->request($method, $this->host . $link, [
                'cookies' => $this->cookies,
                preg_match('/get/i', $method) ? 'query' : 'form_params' => $data,
            ]);
        } catch (\Exception $exception) {
            if ($this->attempt && !is_null($this->auth())) {
                $this->attempt = false;
                return $this->request($method, $link, $data);
            }
            return null;
        } catch (\Error $error) {
            return null;
        }
    }

    /** auth method, run if only need auth
     * @return \GuzzleHttp\Psr7\Response
     * */
    protected function auth()
    {
        try {
            return $this->httpClient->request('POST', $this->host . $_ENV['AMO_AUTH_LINK'] . '?type=json', [
                'cookies' => $this->cookies,
                'form_params' => [
                    'USER_LOGIN' => $_ENV['LOGIN'],
                    'USER_HASH' => $_ENV['API'],
                ],
            ]);
        } catch (\Exception $exception) {
            return null;
        } catch (\Error $error) {
            return null;
        }
    }

    /** convert string resource to array
     * @param string $rawData
     * @return array
     * */
    protected function getArray($rawData)
    {
        if (!empty($rawData)) {
            try {
                return json_decode($rawData->getBody()->getContents(), true);
            } catch (\Exception $exception) { }
        }
        return null;
    }
}